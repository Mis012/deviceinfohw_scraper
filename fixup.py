#!/usr/bin/python3

#spdx GPL-3.0-or-later
#Copyright (C) Mis012 2019

import os

import sqlite3


old_conn = sqlite3.connect('db_master.sqlite')
old_c = old_conn.cursor()

conn = sqlite3.connect('db_fixed.sqlite')
c = conn.cursor()

#("index",MANUFACTURER,BRAND,MODEL,R,PLATFORM,RESOLUTION,LCM,TOUCHSCREEN,CAMERA,LENS,ACCELEROMETER,ALSPS,MAGNETOMETER,GYROSCOPE,SOUND,AUDIO,CHARGER,PMIC,WIFI,FLASH,SCSI,FP,NFC,OTHER,BAROMETER,MODEM,SoC)')

c.execute('CREATE TABLE FIXED (SoC,VENDOR,MODEL,PLATFORM,RESOLUTION,LCM,TOUCHSCREEN,CAMERA,LENS,ACCELEROMETER,ALSPS,MAGNETOMETER,GYROSCOPE,SOUND,AUDIO,CHARGER,PMIC,WIFI,FP,NFC,OTHER,BAROMETER)')

for row in old_c.execute("SELECT * FROM MERGED"):
	c.execute("INSERT INTO FIXED (SoC,VENDOR,MODEL,PLATFORM,RESOLUTION,LCM,TOUCHSCREEN,CAMERA,LENS,ACCELEROMETER,ALSPS,MAGNETOMETER,GYROSCOPE,SOUND,AUDIO,CHARGER,PMIC,WIFI,FP,NFC,OTHER,BAROMETER) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", (row[27], str(row[1]).replace("&nbsp", "")+'\n'+str(row[2]).replace("&nbsp", ""), str(row[3]).replace("&nbsp", ""), str(row[5]).replace("&nbsp", ""), str(row[6]).replace("&nbsp", ""), str(row[7]).replace("&nbsp", ""), str(row[8]).replace("&nbsp", ""), str(row[9]).replace("&nbsp", ""), str(row[10]).replace("&nbsp", ""), str(row[11]).replace("&nbsp", ""), str(row[12]).replace("&nbsp", ""), str(row[13]).replace("&nbsp", ""), str(row[14]).replace("&nbsp", ""), str(row[15]).replace("&nbsp", ""), str(row[16]).replace("&nbsp", ""), str(row[17]).replace("&nbsp", ""), str(row[18]).replace("&nbsp", ""), str(row[19]).replace("&nbsp", ""), str(row[22]).replace("&nbsp", ""), str(row[23]).replace("&nbsp", ""), str(row[24]).replace("&nbsp", ""), str(row[25]).replace("&nbsp", "")))
	conn.commit()
