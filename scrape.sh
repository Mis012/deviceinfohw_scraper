#!/bin/bash

SOC_ID="cpu340"
NUM_PAGES=$(curl "http://deviceinfohw.ru/devices/index.php?platform=platform0&cpu=$SOC_ID&brand=brand0&filter=&submit=Search" | sed "s/href=/\n/g" | sed "s/raquo/raquo\n/" | grep "&raquo" | tr '?' '\n' | tr '&' '\n' | grep page | sed s/page=//)

echo $NUM_PAGES
