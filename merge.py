#!/usr/bin/python3

#spdx GPL-3.0-or-later
#Copyright (C) Mis012 2019

import os

import sqlite3

soc_list = [
	["cpu5", "apq8016"], 
	["cpu11", "apq8064"], 
	["cpu13", "apq8084"], 
	["cpu14", "apq8096"], 
	["cpu16", "aries"], 
	["cpu20", "BCM2835"], 
	["cpu24", "cardhu"], 
	["cpu28", "dalmore"], 
	["cpu29", "eagle"], 
	["cpu63", "harmony"], 
	["cpu77", "koelsch"], 
	["cpu121", "msm8916"], 
	["cpu135", "msm8960"], 
	["cpu136", "msm8960dt"], 
	["cpu137", "msm8960pro"], 
	["cpu138", "msm8974"], 
	["cpu139", "msm8974pro"], 
	["cpu142", "msm8992"], 
	["cpu143", "msm8994"], 
	["cpu144", "msm8996"], 
	["cpu145", "msm8996pro"], 
	["cpu146", "msm8998"], 
	["cpu162", "MT6580"], 
	["cpu163", "MT6580M"], 
	["cpu164", "MT6580WP"], 
	["cpu167", "MT6589"], 
	["cpu169", "MT6592"], 
	["cpu170", "MT6592T"], 
	["cpu202", "MT6755"], 
	["cpu203", "MT6755BM"], 
	["cpu204", "MT6755M"], 
	["cpu205", "MT6755V_B"], 
	["cpu206", "MT6755V_BM"], 
	["cpu207", "MT6755V_C"], 
	["cpu208", "MT6755V_CM"], 
	["cpu209", "MT6755V_CT"], 
	["cpu210", "MT6755V_W"], 
	["cpu211", "MT6755V_WM"], 
	["cpu212", "MT6755V_WS"], 
	["cpu213", "MT6755V_WT"], 
	["cpu246", "MT6795"], 
	["cpu247", "MT6795M"], 
	["cpu248", "MT6795MM"], 
	["cpu249", "MT6795T"], 
	["cpu250", "MT6797"], 
	["cpu251", "MT6797D"], 
	["cpu252", "MT6797M"], 
	["cpu253", "MT6797T"], 
	["cpu254", "MT6797X"], 
	["cpu258", "MT8127"], 
	["cpu259", "MT8135"], 
	["cpu267", "MT8173"], 
	["cpu296", "MT6592"], 
	["cpu310", "ODROID_C2"], 
	["cpu311", "OMAP4430"], 
	["cpu339", "rk3188"], 
	["cpu340", "rk3188plus"], 
	["cpu341", "rk3229"], 
	["cpu342", "rk3288"], 
	["cpu343", "rk3368"], 
	["cpu344", "rk3399"], 
	["cpu345", "rk3399_box"], 
	["cpu376", "sdm845"], 
	["cpu392", "stingray"], 
	["cpu422", "ventana"]
]

conn = sqlite3.connect('db_master.sqlite')
c = conn.cursor()

c.execute('CREATE TABLE MERGED ("index",MANUFACTURER,BRAND,MODEL,R,PLATFORM,RESOLUTION,LCM,TOUCHSCREEN,CAMERA,LENS,ACCELEROMETER,ALSPS,MAGNETOMETER,GYROSCOPE,SOUND,AUDIO,CHARGER,PMIC,WIFI,FLASH,SCSI,FP,NFC,OTHER,BAROMETER,MODEM,SoC)')

for i in range(len(soc_list)):
	soc = soc_list[i][1]

	print("merging "+soc+"...")

	c.execute('ATTACH DATABASE "'+soc+'.db" AS "'+soc+'"')

	c.execute('INSERT INTO MERGED SELECT * FROM "'+soc+'".MERGED')
	conn.commit()

	c.execute('DETACH DATABASE "'+soc+'"')

print ("ALL MERGED!")
