#!/usr/bin/python3

#spdx GPL-3.0-or-later
#Copyright (C) Mis012 2019

import sys

import requests
import lxml.html as lh
import pandas as pd

import sqlite3


if (len(sys.argv) < 4):
	print("usage: " + sys.argv[0] + " <soc_codename> <cpu_id> <num_pages> \n" +
		  "where soc_codename is e.g msm8916 or MT6580, cpu_id is the identificator that deviceinfohw.ru has assigned to that SoC (may change over time), and num_pages is the number of pages that exist for that SoC on deviceinfohw.ru (will change over time)")
	sys.exit()

cpu = number_of_pages = sys.argv[1]
cpu_id = number_of_pages = sys.argv[2]
number_of_pages = int(sys.argv[3])


union_string = "CREATE TABLE MERGED AS "

conn = sqlite3.connect(cpu+'.db')
c = conn.cursor()

for page_num in range(1, (number_of_pages+1)):

	url='http://deviceinfohw.ru/devices/index.php?platform=platform0&cpu=' + cpu_id + '&brand=brand0&submit=Search&page=' + str(page_num)

	#Create a handle, page, to handle the contents of the website
	page = requests.get(url)

	#Store the contents of the website under doc
	doc = lh.fromstring(page.content)

	#Parse data that are stored between <tr>..</tr> of HTML
	tr_elements = doc.xpath('//tr')

	#Check the length of the first 12 rows
	[len(T) for T in tr_elements[:12]]

	tr_elements = doc.xpath('//tr')

	#Create empty list
	col=[]
	i=0

	#For each row, store each first element (header) and an empty list
	for t in tr_elements[0]:
		i+=1
		name=t.text_content()
		#print '%d:"%s"'%(i,name)
		col.append((name,[]))
		
	#Since out first row is the header, data is stored on the second row onwards
	for j in range(1,len(tr_elements)):
		#T is our j'th row
		T=tr_elements[j]
		
		#If row is not of size 26, the //tr data is not from our table 
		if len(T)!=26:
		    break
		
		#i is the index of our column
		i=0
		
		#Iterate through each element of the row
		for t in T.iterchildren():
		    data=t.text_content() 
		    #Check if row is empty
		    if i>0:
		    #Convert any numerical value to integers
		        try:
		            data=int(data)
		        except:
		            pass
		    #Append the data to the empty list of the i'th column
		    col[i][1].append(data)
		    #Increment i for the next column
		    i+=1
		    
	Dict={title:column for (title,column) in col}
	df=pd.DataFrame(Dict)

	df.head()
	print("page " + str(page_num) + " scraped")

	c.execute('CREATE TABLE '+cpu+'_'+str(page_num)
			  +' (ACCELEROMETER,ALSPS,AUDIO,BAROMETER,BRAND,CAMERA,CHARGER,FLASH,FP,GYROSCOPE,LCM,LENS,MAGNETOMETER,MANUFACTURER,MODEL,MODEM,NFC,OTHER,PLATFORM,PMIC,R,RESOLUTION,SCSI,SOUND,TOUCHSCREEN,WIFI)')
	conn.commit()

	df.to_sql(cpu+'_'+str(page_num), conn, if_exists='replace')

	union_string += "SELECT * FROM "+cpu+"_"+str(page_num)+" "

	if page_num < number_of_pages :
		union_string += "UNION ALL "

print("- merging pages...")

c.execute(union_string)
conn.commit()

print("- deleting temporary tables...")

for page_num in range(1, (number_of_pages+1)):
	c.execute("DROP TABLE "+cpu+"_"+str(page_num))
	conn.commit()

print("- adding SoC column... -")
c.execute("ALTER TABLE MERGED ADD SoC TEXT")
c.execute("UPDATE MERGED SET SoC = \""+cpu+"\"")
conn.commit()

print("!!!done!!!")
